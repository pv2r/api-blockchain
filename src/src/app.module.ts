import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import  configuration from './Infrastructure/Database/database.config';
import {ControllerModule} from './Aplication/Controller/controller.module';
import { DomainModule } from './Domain/domain.module';
import {RepositoryModule} from "./Infrastructure/Repository/repository.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration]
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.API_WALLET_DATABASE_HOST,
      port: parseInt(process.env.API_WALLET_DATABASE_PORT),
      username: process.env.API_WALLET_DATABASE_USERNAME,
      password: process.env.API_WALLET_DATABASE_PASSWORD,
      database: process.env.API_WALLET_DATABASE,
      synchronize: false,
      migrationsRun: true,
      migrations: ['src/Infrastructure/migration/*{.ts,.js}'],
      cli: {
        migrationsDir: 'src/Infrastructure/migration/*{.ts,.js}'
      },
      entities: [],
      autoLoadEntities: true,
    }),
    DomainModule,
    RepositoryModule,
    ControllerModule
  ],
})
export class AppModule {}
