import {RespostaDto} from "./response.dto";

export interface ApiResponseInterface {
    resposta(array: any[], message: string, success: boolean): RespostaDto
}