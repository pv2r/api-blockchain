import {ApiResponseInterface} from "./response.interface";
import {Injectable} from "@nestjs/common";
import { RespostaDto} from "./response.dto";

@Injectable()
export class BaseApiService implements ApiResponseInterface{
    public resposta(data: any, message: string, success: boolean): RespostaDto {
        return new RespostaDto(data, message,success);
    }

    public respostaPersonalizada(data: any[], mapFunction: any, message: string, success: boolean): RespostaDto{
        return new RespostaDto(mapFunction(data), message, success)
    }
}