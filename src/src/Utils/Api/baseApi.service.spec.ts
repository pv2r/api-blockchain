import { Test } from "@nestjs/testing";
import { BaseApiService } from "./baseApi.service";
import { RespostaDto } from "./response.dto";


describe('gerarCarteiraService', () => {
  let baseService: BaseApiService;
  const result = new RespostaDto([], "222", true);;
  let resposta: RespostaDto


  beforeEach(async () => {
      const module = await Test.createTestingModule({
        providers: [
            {
                provide: BaseApiService,
                useValue: {
                    resposta: jest.fn(() => RespostaDto) 
                }
            }
        ]
      }).compile()
      baseService = module.get(BaseApiService)
      jest.spyOn(baseService, 'resposta').mockImplementation(() => result);
      resposta = baseService.resposta([], "222", true);
  });

  describe('resposta', () => {
    it('Deve retornar um objeto do tipo RespostaDto', async () => {
      expect(resposta).toBe(result);
    });
    it('resposta.data deve ser um array', () => {
        expect(resposta.data).toStrictEqual([])
    })
    it('resposta.message deve ser uma string', () => {
        expect(resposta.message).toStrictEqual("222")
    })
    it('resposta.success deve ser um boolean', () => {
        expect(resposta.success).toStrictEqual(true)
    })
  });
});