import { Module } from '@nestjs/common';
import {BaseApiService} from "./baseApi.service";

@Module({
    imports: [BaseApiService],
    providers: [BaseApiService],
    exports: [BaseApiService],
})
export class ApiUtilsModule {}