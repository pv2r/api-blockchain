import { Module } from '@nestjs/common';
import {ApiUtilsModule} from "./Api/utils.module";

@Module({
    imports: [
        ApiUtilsModule,
    ],
    exports: [
        ApiUtilsModule,
    ],
})
export class UtilsModule {}