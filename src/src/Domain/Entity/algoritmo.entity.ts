import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    Index,
    ManyToMany,
    JoinTable
} from 'typeorm';
import {CarteiraEntity} from "./carteira.entity";
import { MasterKeyEntity } from './masterKey.entity';
import { RedeEntity } from './rede.entity';

@Entity("algoritmos")
export class AlgoritmoEntity {
    @PrimaryGeneratedColumn("uuid")
    algoritmo_uid: string;

    @Column()
    @Index()
    nome: string;

    @Column()
    nome_exibicao: string;

    @Column({
        nullable: true,
        select: false
    })
    created_by: string;

    @Column({
        nullable: true,
        select: false
    })
    updated_by: string;

    @Column({
        nullable: true,
        select: false
    })
    deleted_by: string;

    @CreateDateColumn({
        select: false
    })
    created_at: string

    @UpdateDateColumn({
        select: false
    })
    updated_at: string

    @DeleteDateColumn({
        select: false
    })
    deleted_at: string

    /* relations */
    @OneToMany(type => CarteiraEntity, carteira => carteira.algoritmo)
    carteiras: CarteiraEntity[];
    @OneToMany(type => RedeEntity, rede => rede.algoritmo)
    redes: RedeEntity[];

    @ManyToMany(type => MasterKeyEntity,  master_keys => master_keys.algoritmos)
    @JoinTable({
        name: 'algoritmos_master_keys',
        joinColumn: { name: "algoritmo_uid", referencedColumnName: "algoritmo_uid" },
        inverseJoinColumn: {
          name: "master_key_uid",
          referencedColumnName: "uid"
        }
      })
    master_keys: MasterKeyEntity[]
}