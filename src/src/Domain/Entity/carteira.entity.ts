import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn, DeleteDateColumn, ManyToMany, JoinTable, OneToMany, OneToOne
} from 'typeorm';
import { AlgoritmoEntity } from './algoritmo.entity';

@Entity("carteiras")
export class CarteiraEntity {
  @PrimaryGeneratedColumn("uuid")
  carteira_uid: string;

  @Column()
  @Index()
  endereco: string;

  @Column()
  @Index()
  chave_privada: string;

  @Column()
  @Index()
  chave_publica: string;
  
  @Column()
  algoritmo_uid: string;

  @Column({
    nullable: true,
    select: false
  })
  created_by: string;

  @Column({
    nullable: true,
    select: false
  })
  updated_by: string;

  @Column({
    nullable: true,
    select: false
  })
  deleted_by: string;

  @CreateDateColumn({
    select: false
  })
  created_at: string

  @UpdateDateColumn({
    select: false
  })
  updated_at: string

  @DeleteDateColumn({
    select: false
  })
  deleted_at: string

  /* relations */
  @ManyToOne(type => AlgoritmoEntity, algoritmo => algoritmo.carteiras)
  @JoinColumn({ name: "algoritmo_uid" })
  algoritmo: AlgoritmoEntity;

  
}