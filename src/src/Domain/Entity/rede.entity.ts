import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    Index,
    JoinColumn,
    ManyToOne
} from 'typeorm';
import { AlgoritmoEntity } from './algoritmo.entity';

@Entity("redes")
export class RedeEntity {
    @PrimaryGeneratedColumn("uuid")
    rede_uid: string;

    @Column()
    @Index()
    nome: string;

    @Column()
    nome_exibicao: string;

    @Column()
    provider: string;

    @Column()
    @Index()
    moeda_principal: string;

    @Column({
        nullable: true,
        select: false
    })
    created_by: string;

    @Column()
    algoritmo_uid: string;
    
    @Column({
        nullable: true,
        select: false
    })
    updated_by: string;

    @Column({
        nullable: true,
        select: false
    })
    deleted_by: string;

    @CreateDateColumn({
        select: false
    })
    created_at: string

    @UpdateDateColumn({
        select: false
    })
    updated_at: string
    
    @DeleteDateColumn()
    deleted_at: string

    /* relations */
    @ManyToOne(type => AlgoritmoEntity, algoritmo => algoritmo.redes)
    @JoinColumn({ name: "algoritmo_uid" })
    algoritmo: AlgoritmoEntity;
}