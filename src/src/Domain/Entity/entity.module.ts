import { Module } from '@nestjs/common';
import { AlgoritmoEntity } from './algoritmo.entity';
import {CarteiraEntity} from "./carteira.entity";
import { RedeEntity } from './rede.entity';
import {TransacaoEntity} from "./transacao.entity";

const entities = [
    CarteiraEntity,
    RedeEntity,
    TransacaoEntity,
    AlgoritmoEntity
]
@Module({
    imports: entities,
    exports: entities,
})
export class EntityModule {}