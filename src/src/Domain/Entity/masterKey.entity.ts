import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn, DeleteDateColumn, ManyToMany, JoinTable
} from 'typeorm';
import { AlgoritmoEntity } from './algoritmo.entity';

@Entity("master_keys")
export class MasterKeyEntity {
  @PrimaryGeneratedColumn("uuid")
  uid: string;

  @Column()
  @Index()
  master_key: string;

  @Column()
  mnemonic: string;

  created_by: string;

  @Column({
    nullable: true,
    select: false
  })
  updated_by: string;

  @Column({
    nullable: true,
    select: false
  })
  deleted_by: string;

  @CreateDateColumn({
    select: false
  })
  created_at: string

  @UpdateDateColumn({
    select: false
  })
  updated_at: string

  @DeleteDateColumn({
    select: false
  })
  deleted_at: string

  @ManyToMany(type => AlgoritmoEntity, { eager: true })
  @JoinTable({
    name: 'algoritmos_master_keys',
    joinColumn: { name: "master_key_uid", referencedColumnName: "uid" },
    inverseJoinColumn: {
      name: "algoritmo_uid",
      referencedColumnName: "algoritmo_uid"
    }
  })
  algoritmos: AlgoritmoEntity[]

  
}