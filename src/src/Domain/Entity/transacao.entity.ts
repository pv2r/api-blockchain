import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    Index,
    ManyToMany
} from 'typeorm';
import {CarteiraEntity} from "./carteira.entity";

@Entity("transacoes")
export class TransacaoEntity {
    @PrimaryGeneratedColumn("uuid")
    transacao_uid: string;

    @Column()
    to: string;

    @Column()
    from: string;

    @Column()
    hash: string;

    @Column()
    data: string;

    @Column({
        nullable: true,
        select: false
    })
    created_by: string;

    @Column({
        nullable: true,
        select: false
    })
    updated_by: string;

    @Column({
        nullable: true,
        select: false
    })
    deleted_by: string;

    @CreateDateColumn({
        select: false
    })
    created_at: string

    @UpdateDateColumn({
        select: false
    })
    updated_at: string

    @DeleteDateColumn({
        select: false
    })
    deleted_at: string
}