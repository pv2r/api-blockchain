import { Injectable } from "@nestjs/common";
import { MasterKeyEntity } from "../../../Domain/Entity/masterKey.entity";
import { MasterKeyRepository } from "../../../Infrastructure/Repository/MasterKey/masterKey.repository";

@Injectable()
export class MasterKeyService {
    constructor(
        private masterKeyRepository: MasterKeyRepository,
    ){}

    public async findAll(): Promise<MasterKeyEntity[]> {
        return this.masterKeyRepository.findAll()
    }

}