import { Module } from '@nestjs/common';
import { MasterKeyPersistenceModule } from '../../../Infrastructure/Repository/MasterKey/masterKey.persistence.module';
import { MasterKeyService } from './masterKey.service';


@Module({
    imports: [MasterKeyPersistenceModule],
    exports: [MasterKeyService],
    providers: [MasterKeyService],
})
export class MasterKeyModule {}