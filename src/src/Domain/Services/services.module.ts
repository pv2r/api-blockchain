import { Module } from '@nestjs/common';
import {GerarCarteiraModule} from "./GerarCarteira/gerarCarteira.module";
import {BlockchainModule} from "./Blockchain/blockchain.module";
import {TransacaoModule} from "./Transacao/transacao.module";
import { MasterKeyModule } from './MasterKey/masterKey.module';
import { RedeModule } from './Rede/rede.module';

const modules = [
    GerarCarteiraModule,
    BlockchainModule,
    RedeModule,
    TransacaoModule,
    MasterKeyModule
]
@Module({
    imports: modules,
    exports: modules
})
export class ServiceModule {}