import { IsNotEmpty, IsString, Validate } from "class-validator";
import {AlgoritmoEntity} from "../../Entity/algoritmo.entity";
import { ValidaAlgoritmoUid } from "../Validators/algoritmoUid.validator";

export class CarteiraDto {
    @Validate(ValidaAlgoritmoUid)

    @IsString()
    @IsNotEmpty()
    public algoritmo: AlgoritmoEntity

    constructor(
        public endereco: string,
        public chave_publica: string,
        public chave_privada: string,
        algoritmo: AlgoritmoEntity
       
    ){
        this.algoritmo = algoritmo
    }
}