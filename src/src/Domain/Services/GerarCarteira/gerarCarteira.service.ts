import {Injectable} from "@nestjs/common";
import {randomBytes, createHash} from "crypto";
import {ethers} from "ethers";
import {CarteiraRepository} from "../../../Infrastructure/Repository/Carteira/carteira.repository";
import {CarteiraModel} from "../../../Infrastructure/models/carteira.model";
import {CarteiraDto} from "./carteira.dto";
import {GerarCarteiraInterface} from "./gerar.carteira.interface";
import { AlgoritmoEntity } from "../../../Domain/Entity/algoritmo.entity";
import { MasterKeyRepository } from "../../../Infrastructure/Repository/MasterKey/masterKey.repository";
import { AlgoritmoRepository } from "../../../Infrastructure/Repository/Algoritmo/algoritmo.repository";
@Injectable()
export class GerarCarteiraService implements GerarCarteiraInterface {
    constructor(
        private carteiraRepository: CarteiraRepository,
        private masterKeyRepository: MasterKeyRepository,
        private algoritmoRepository: AlgoritmoRepository
    ){}

    async gerarCarteira(algoritmo: AlgoritmoEntity, created_by: string): Promise<CarteiraModel> {
        const carteiraDto = await this.criarCarteira(algoritmo)

        const carteiraModel = new CarteiraModel(
            carteiraDto.endereco,
            carteiraDto.chave_publica,
            carteiraDto.chave_privada,
            carteiraDto.algoritmo,
            created_by
        )
        await this.carteiraRepository.store(carteiraModel.toObject())
        return carteiraModel
    }

    private async criarCarteira(algoritmo: AlgoritmoEntity): Promise<CarteiraDto> {
        // recuperar a master key do algoritmo selecionado
        const algoritmoEntity = await this.algoritmoRepository.findOne(algoritmo)
        // pegando a última master key disponível
        const masterKeyEntity = algoritmoEntity.master_keys[algoritmoEntity.master_keys.length - 1]
        const pathNumber = await this.carteiraRepository.countCarteirasPorAlgoritmo(algoritmoEntity.algoritmo_uid)
        const carteira = await ethers.Wallet.fromMnemonic(masterKeyEntity.mnemonic, `m/44'/60'/0'/0/${pathNumber}`);

        return new CarteiraDto(
            carteira.address,
            carteira.publicKey,
            carteira.privateKey,
            algoritmo,
        )
    }
}