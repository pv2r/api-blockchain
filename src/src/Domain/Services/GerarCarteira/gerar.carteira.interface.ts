import {CarteiraModel} from "../../../Infrastructure/models/carteira.model";
import { AlgoritmoEntity } from "../../../Domain/Entity/algoritmo.entity";

export interface GerarCarteiraInterface {
     gerarCarteira(algoritmo: AlgoritmoEntity, created_by: string): Promise<CarteiraModel>
}
