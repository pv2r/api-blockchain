import { Module } from '@nestjs/common';
import {GerarCarteiraService} from "./gerarCarteira.service";
import {CarteiraPersistenceModule} from "../../../Infrastructure/Repository/Carteira/carteira.persistence.module";
import {RedePersistenceModule} from "../../../Infrastructure/Repository/Rede/rede.persistence.module";
import { MasterKeyPersistenceModule } from '../../../Infrastructure/Repository/MasterKey/masterKey.persistence.module';
import { AlgoritmoPersistenceModule } from '../../../Infrastructure/Repository/Algoritmo/algoritmo.persistence.module';

@Module({
    imports: [CarteiraPersistenceModule, MasterKeyPersistenceModule, AlgoritmoPersistenceModule],
    providers: [
        {
            provide: 'GerarCarteiraInterface',
            useClass: GerarCarteiraService
        }
    ],
    exports: ['GerarCarteiraInterface', CarteiraPersistenceModule, MasterKeyPersistenceModule, AlgoritmoPersistenceModule],
})
export class GerarCarteiraModule {}