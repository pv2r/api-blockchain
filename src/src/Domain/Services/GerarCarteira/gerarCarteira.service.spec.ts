import { Test } from "@nestjs/testing";
import { AlgoritmoEntity } from "../../../Domain/Entity/algoritmo.entity";
import { CarteiraModel } from "../../../Infrastructure/models/carteira.model";
import { MasterKeyService } from "../MasterKey/masterKey.service";
import { GerarCarteiraService } from "./gerarCarteira.service";

describe("GerarCarteiraService", () => {
  let service: GerarCarteiraService
beforeAll( async() => {
  const mockGerarCarteiraService: Partial<GerarCarteiraService> = {
    gerarCarteira: (algoritmo: AlgoritmoEntity, created_by: string) => Promise.resolve(new CarteiraModel("endereco", "chave_publica", "chave_privada", new AlgoritmoEntity())),
  }

  const module = await Test.createTestingModule({
    providers: [
      GerarCarteiraService,
      {
        provide: GerarCarteiraService,
        useValue: mockGerarCarteiraService
      }
    ], 
  }).compile()

  service = module.get(GerarCarteiraService)
})

  it('Consegue criar uma instância do GerarCarteiraService', async () => {
    expect(service).toBeDefined()
  })

  it("Consegue criar uma carteira", async () => {
    const wallet = await service.gerarCarteira(new AlgoritmoEntity(), 'fintools')

    expect(wallet).toBeDefined()
    expect(wallet.endereco).toBeDefined()
    expect(wallet.chave_publica).toBeDefined()
    expect(wallet.algoritmo_uid).toBeDefined()
    expect(wallet.chave_privada).toBeDefined()
    expect(wallet).toBeInstanceOf(CarteiraModel)
    
  })
})