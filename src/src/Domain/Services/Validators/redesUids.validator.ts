import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { RedeEntity } from '../../../Domain/Entity/rede.entity';
import { getConnection } from 'typeorm';

@ValidatorConstraint({ name: 'validaRedesUids', async: false })
export class ValidaRedesUids implements ValidatorConstraintInterface {
  async validate(redesUids: Array<RedeEntity>, args: ValidationArguments) {
      const redes = await getConnection()
      .getRepository(RedeEntity)
      .createQueryBuilder("rede")
      .where("rede.rede_uid IN (:...redes)", { redes: redesUids })
      .getMany();

      
      return redes.length === redesUids.length
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Invalid network(s) were sent';
  }
}