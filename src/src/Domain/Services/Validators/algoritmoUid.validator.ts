import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { AlgoritmoEntity } from '../../../Domain/Entity/algoritmo.entity';
import { getConnection } from 'typeorm';

@ValidatorConstraint({ name: 'validaRedesUids', async: false })
export class ValidaAlgoritmoUid implements ValidatorConstraintInterface {
  async validate(algoritmo_uid: AlgoritmoEntity, args: ValidationArguments) {
      const algoritmoValido = await getConnection()
      .getRepository(AlgoritmoEntity)
      .createQueryBuilder("algoritmo")
      .where("algoritmo.algoritmo_uid = :uid", { uid: algoritmo_uid })
      .getOne();
      
      return algoritmoValido ? true: false
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Invalid Algorithm';
  }
}