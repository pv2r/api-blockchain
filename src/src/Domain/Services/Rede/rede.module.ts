import { Module } from '@nestjs/common';
import { RedePersistenceModule } from '../../../Infrastructure/Repository/Rede/rede.persistence.module';
import { RedeService } from './rede.service';


@Module({
    imports: [RedePersistenceModule],
    exports: [RedeService],
    providers: [RedeService],
})
export class RedeModule {}