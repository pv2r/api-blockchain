import { Injectable } from "@nestjs/common";
import { RedeEntity } from "../../../Domain/Entity/rede.entity";
import { RedeModel } from "../../../Infrastructure/models/rede.model";
import { RedeRepository } from "../../../Infrastructure/Repository/Rede/rede.repository";

@Injectable()
export class RedeService {
    constructor(
        private redeRepository: RedeRepository,
    ){}

    public async findAll(): Promise<RedeEntity[]> {
        return this.redeRepository.findAll()
    }

    public async store(rede: RedeModel):  Promise<RedeEntity> {
        return await this.redeRepository.save(rede)
    }

    public async delete(rede_uid: string): Promise<RedeEntity>{
        return await this.redeRepository.delete(rede_uid)
    }

}