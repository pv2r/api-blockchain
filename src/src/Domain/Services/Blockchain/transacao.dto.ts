import { IsNotEmpty, IsNumber, IsString } from "class-validator"
import { BigNumber } from "ethers"

export class TransacaoDto{
    @IsString({
        message: `O campo $property deve ser uma string`
    })
    @IsNotEmpty({
        message: `O campo $property não pode estar vazio`
    })
    to: string

    @IsString({
        message: `O campo $property deve ser uma string`
    })
    @IsNotEmpty({
        message: `O campo $property não pode estar vazio`
    })
    from: string

    // @IsString()
    // @IsNotEmpty({
    //     message: `O campo $property não pode estar vazio`
    // })
    // gasPrice: string

    // @IsString()
    // @IsNotEmpty({
    //     message: `O campo $property não pode estar vazio`
    // })
    // gasLimit: string

    // @IsString()
    // @IsNotEmpty({
    //     message: `O campo $property não pode estar vazio`
    // })
    // nonce: string

    @IsString()
    @IsNotEmpty({
        message: `O campo $property não pode estar vazio`
    })
    value: string

    @IsString({
        message: `O campo $property deve ser uma string`
    })
    @IsNotEmpty({
        message: `O campo $property não pode estar vazio`
    })
    data: string

}