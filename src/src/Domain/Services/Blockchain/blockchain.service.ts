import { Injectable } from "@nestjs/common";
import { ethers } from "ethers";
import { TransacaoDto } from "./transacao.dto";
import {TransacaoRepository} from "../../../Infrastructure/Repository/Transacao/transacao.repository";
import {TransacaoModel} from "../../../Infrastructure/models/transacao.model";
import {CarteiraRepository} from "../../../Infrastructure/Repository/Carteira/carteira.repository";

@Injectable()
export class BlockchainService {
    constructor(
        private carteiraRepository: CarteiraRepository,
        private transacaoRepository: TransacaoRepository
    ){}
    public async enviarBloco(transacao: TransacaoDto){
        const carteiraEntity = await this.carteiraRepository.findCarteiraPorEndereco(transacao.from)
        const networkUrl = process.env.API_BLOCKCHAIN_NETWORK_URL
        const carteira = await new ethers.Wallet(carteiraEntity.chave_privada)
        const provider = await new ethers.providers.JsonRpcProvider(networkUrl)
        const carteiraConectada = await carteira.connect(provider)
        const respostaTransacao = await carteiraConectada.sendTransaction(transacao)
        const transacaoModel = new TransacaoModel(
            respostaTransacao.to,
            respostaTransacao.from,
            respostaTransacao.hash,
            respostaTransacao.data
        )
        await this.transacaoRepository.store(transacaoModel)
        return respostaTransacao
    }
}