import { Type } from "class-transformer"
import { IsNotEmpty, IsObject, IsString, ValidateNested } from "class-validator"
import { TransacaoDto } from "./transacao.dto"

export class StoreDto {
    // @IsString({
    //     message: "O campo endereço deve ser uma string"
    // })
    // @IsNotEmpty({
    //     message: "O campo endereço não pode estar vazio"
    // })
    // endereco: string
    
    @IsObject()
    @ValidateNested()
    @Type(() => TransacaoDto)
    transacao: TransacaoDto
   
}
