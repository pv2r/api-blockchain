import {Module} from "@nestjs/common";
import {BlockchainService} from "./blockchain.service";
import {CarteiraPersistenceModule} from "../../../Infrastructure/Repository/Carteira/carteira.persistence.module";
import {TransacaoReposityModule} from "../../../Infrastructure/Repository/Transacao/transacao.repository.module";

@Module({
    imports: [CarteiraPersistenceModule, TransacaoReposityModule],
    providers: [BlockchainService],
    exports: [BlockchainService,CarteiraPersistenceModule]
})
export class BlockchainModule {}