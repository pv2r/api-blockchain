import {Injectable} from "@nestjs/common";
import {CarteiraRepository} from "../../../Infrastructure/Repository/Carteira/carteira.repository";
import {TransacaoRepository} from "../../../Infrastructure/Repository/Transacao/transacao.repository";
import {TransacaoDto} from "./Dto/transacao.dto";
import {ethers} from "ethers";
import {RedeRepository} from "../../../Infrastructure/Repository/Rede/rede.repository";

@Injectable()
export class AssinarTransacaoAction {
    constructor(
        private carteiraRepository: CarteiraRepository,
        private redeRepository: RedeRepository
    ){}

    public async executar(transacao: TransacaoDto)
    {
        const carteiraEntity = await this.carteiraRepository.findCarteiraPorEndereco(transacao.from)
        const networkUrl = (await this.redeRepository.findOne(transacao.network_uid)).provider
        delete transacao.network_uid
        const carteira = await new ethers.Wallet(carteiraEntity.chave_privada)
        const provider = await new ethers.providers.JsonRpcProvider(networkUrl)
        const carteiraConectada = await carteira.connect(provider)
        return carteiraConectada.signTransaction(transacao).then((data) => {
            return {
                "signed_transaction": data
            }
        })
    }
}