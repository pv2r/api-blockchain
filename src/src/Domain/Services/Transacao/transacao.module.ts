import {Module} from "@nestjs/common";
import {AssinarTransacaoAction} from "./assinar-transacao-action.service";
import {CarteiraPersistenceModule} from "../../../Infrastructure/Repository/Carteira/carteira.persistence.module";
import {RepositoryModule} from "../../../Infrastructure/Repository/repository.module";

@Module({
    imports: [ CarteiraPersistenceModule, RepositoryModule ],
    providers: [ AssinarTransacaoAction ],
    exports: [ AssinarTransacaoAction ]
})
export class TransacaoModule {}