import { Test } from "@nestjs/testing";
import { TransacaoDto } from "./Dto/transacao.dto";
import { AssinarTransacaoAction } from "./assinar-transacao-action.service";


describe("AssinarTransacaoAction", () => {
  let service: AssinarTransacaoAction
beforeAll( async() => {
  const mockAssinarTransacaoService: Partial<AssinarTransacaoAction> = {
    executar: (algoritmo: TransacaoDto) => Promise.resolve({'signed_transaction': "teste"}),
  }
  const mockTransacaoDto: Partial<TransacaoDto> = {
    to: 'to',
    from: 'from',
    value: 'value',
    data: 'data',
  }

  const module = await Test.createTestingModule({
    providers: [
      AssinarTransacaoAction,
      {
        provide: AssinarTransacaoAction,
        useValue: mockAssinarTransacaoService
      },
      {
          provide: TransacaoDto,
          useValue: mockTransacaoDto
      }
    ], 
  }).compile()

  service = module.get(AssinarTransacaoAction)
})

  it('Consegue criar uma instância de AssinarTransacaoAction', async () => {
    expect(service).toBeDefined()
  })

  it("Consegue executar", async () => {
    const transacaoDto = new TransacaoDto()
    const mockTransacaoAssinada = await service.executar(transacaoDto)
    
    expect(mockTransacaoAssinada).toBeDefined()
    expect(mockTransacaoAssinada.signed_transaction).toBeDefined()
    expect(mockTransacaoAssinada.signed_transaction).toBe("teste")
    
  })
})