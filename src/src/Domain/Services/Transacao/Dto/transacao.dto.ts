import {IsNotEmpty, IsOptional, IsString} from "class-validator";

export class TransacaoDto {
    @IsString()
    @IsNotEmpty()
    network_uid: string
    @IsString()
    @IsNotEmpty()
    to: string
    @IsString()
    @IsNotEmpty()
    from: string
    @IsString()
    @IsOptional()
    gasPrice: string
    @IsOptional()
    gasLimit: string
    @IsString()
    @IsOptional()
    nonce: string
    @IsString()
    @IsNotEmpty()
    value: string
    @IsString()
    @IsNotEmpty()
    data: string
}