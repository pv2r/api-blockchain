import { Module } from '@nestjs/common';
import {ServiceModule} from "./Services/services.module";

@Module({
	imports: [ServiceModule],
	exports: [ServiceModule],
})

export class DomainModule {}