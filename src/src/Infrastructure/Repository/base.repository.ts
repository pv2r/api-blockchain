import { Injectable } from '@nestjs/common';
@Injectable()
export class BaseRepository {
    constructor(
        private repository: any
    ){}
    async findAll(params? : Object): Promise<any[]> {
        return this.repository.find(params);
    }
    async findOne(params): Promise<any> {
        return await this.repository.findOne(params);
    }
    async store(data: any): Promise<any> {
        return await this.repository.save(data)
    }
}
