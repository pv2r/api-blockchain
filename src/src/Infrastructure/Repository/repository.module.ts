import { Module } from '@nestjs/common';
import { AlgoritmoPersistenceModule } from './Algoritmo/algoritmo.persistence.module';
import { CarteiraPersistenceModule } from "./Carteira/carteira.persistence.module";
import { MasterKeyPersistenceModule } from './MasterKey/masterKey.persistence.module';
import { RedePersistenceModule } from "./Rede/rede.persistence.module";
import {TransacaoReposityModule} from "./Transacao/transacao.repository.module";

const modules = [
    CarteiraPersistenceModule,
    RedePersistenceModule,
    TransacaoReposityModule,
    AlgoritmoPersistenceModule,
    MasterKeyPersistenceModule
]

@Module({
    imports: modules,
    exports: modules
})

export class RepositoryModule {}