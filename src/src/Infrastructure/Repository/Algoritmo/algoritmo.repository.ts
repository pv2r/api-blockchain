import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {AlgoritmoEntity} from "../../../Domain/Entity/algoritmo.entity";

@Injectable()
export class AlgoritmoRepository {
  constructor(
    @InjectRepository(AlgoritmoEntity)
    private algoritmoRepository: Repository<AlgoritmoEntity>,
    ){}

  async findOne(algoritmo_uid): Promise<AlgoritmoEntity> {
    return this.algoritmoRepository.findOne(algoritmo_uid, {
      relations: ['master_keys']
    })
  }
}
