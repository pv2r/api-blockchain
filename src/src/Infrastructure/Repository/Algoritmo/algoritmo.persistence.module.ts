import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlgoritmoRepository} from './algoritmo.repository';
import {AlgoritmoEntity} from "../../../Domain/Entity/algoritmo.entity";

@Module({
  imports: [TypeOrmModule.forFeature([AlgoritmoEntity])],
  providers: [AlgoritmoRepository],
  exports: [AlgoritmoRepository],
})
export class AlgoritmoPersistenceModule {}