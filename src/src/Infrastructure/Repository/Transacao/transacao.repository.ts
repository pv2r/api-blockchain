import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {BaseRepository} from "../base.repository";
import {TransacaoEntity} from "../../../Domain/Entity/transacao.entity";

@Injectable()
export class TransacaoRepository extends BaseRepository{
    constructor(
        @InjectRepository(TransacaoEntity)
        private myRepository: Repository<TransacaoEntity>,
    ){
        super(myRepository)
    }
}
