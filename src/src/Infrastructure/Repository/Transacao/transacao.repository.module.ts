import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransacaoRepository } from './transacao.repository';
import {TransacaoEntity} from "../../../Domain/Entity/transacao.entity";

@Module({
    imports: [TypeOrmModule.forFeature([TransacaoEntity])],
    providers: [TransacaoRepository],
    exports: [TransacaoRepository],
})
export class TransacaoReposityModule {}