import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CarteiraDto } from '../../../Domain/Services/GerarCarteira/carteira.dto';
import { Repository } from 'typeorm';
import {CarteiraEntity} from "../../../Domain/Entity/carteira.entity";

@Injectable()
export class CarteiraRepository {
  constructor(
    @InjectRepository(CarteiraEntity)
    private carteiraRepository: Repository<CarteiraEntity>,
    ){}

  async findAll(carteiraDto: CarteiraDto): Promise<CarteiraEntity[]> {    
    return await this.carteiraRepository.find({
      where: carteiraDto,
      relations: ['algoritmo'],
    });
  }

  mapeiaResposta(carteiras: CarteiraEntity[], campos: Object){
    return (carteiras: CarteiraDto[]) => {
      return carteiras.map((carteira: CarteiraDto) => {
        return campos
      });
    }
  }

  async findOne(carteiraDto: CarteiraDto): Promise<CarteiraEntity> {
    return await this.carteiraRepository.findOne({
      where: {
        endereco: carteiraDto.endereco,
      },
      relations: ['algoritmo']
    });
  }

  async findCarteiraPorEndereco(endereco: string): Promise<CarteiraEntity> {
    return await this.carteiraRepository.findOne({
      where: {
        endereco: endereco,
      },
      relations: ['algoritmo']
    });
  }

  async countCarteirasPorAlgoritmo(algoritmo: string): Promise<Number>{
    return this.carteiraRepository.count({
      where: {
        algoritmo: {algoritmo_uid: algoritmo}
      },
      relations: ['algoritmo']
    })
  }

  async store(carteira: any) {
    // TODO: criar o repository específico para carteira para flexibilizar a passagem de tipo para o save
    return await this.carteiraRepository.save(carteira)
  }
}
