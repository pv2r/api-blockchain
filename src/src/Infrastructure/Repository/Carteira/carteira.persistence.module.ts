import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarteiraRepository } from './carteira.repository';
import {CarteiraEntity} from "../../../Domain/Entity/carteira.entity";

@Module({
  imports: [TypeOrmModule.forFeature([CarteiraEntity])],
  providers: [CarteiraRepository],
  exports: [CarteiraRepository],
})
export class CarteiraPersistenceModule {}