import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {MasterKeyEntity } from "../../../Domain/Entity/masterKey.entity";
import { MasterKeyRepository } from './masterKey.repository';

@Module({
  imports: [TypeOrmModule.forFeature([MasterKeyEntity])],
  providers: [MasterKeyRepository],
  exports: [MasterKeyRepository],
})
export class MasterKeyPersistenceModule {}