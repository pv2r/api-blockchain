import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MasterKeyEntity } from '../../../Domain/Entity/masterKey.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MasterKeyRepository {
  constructor(
    @InjectRepository(MasterKeyEntity)
    private masterKeyRepository: Repository<MasterKeyEntity>,
    ){}

    async findAll(): Promise<MasterKeyEntity[]>{
        return await this.masterKeyRepository.find({
          relations: ['algoritmos']
        })
    }

}
