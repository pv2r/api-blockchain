import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedeRepository } from './rede.repository';
import {RedeEntity} from "../../../Domain/Entity/rede.entity";
import { AlgoritmoEntity } from '../../../Domain/Entity/algoritmo.entity';

@Module({
    imports: [TypeOrmModule.forFeature([RedeEntity])],
    providers: [RedeRepository],
    exports: [RedeRepository],
})
export class RedePersistenceModule {}