import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import {RedeEntity} from "../../../Domain/Entity/rede.entity";
import {BaseRepository} from "../base.repository";

@Injectable()
export class RedeRepository extends BaseRepository{
    constructor(
        @InjectRepository(RedeEntity)
        private redeRepository: Repository<RedeEntity>,
    ){
        super(redeRepository)
    }

    async findOne(rede_uid: string): Promise<RedeEntity> {
        return this.redeRepository.findOne(rede_uid)
    }

    async save(rede: any): Promise<RedeEntity>{
        return this.redeRepository.save(rede)
    }

    async delete(rede_uid): Promise<any>{
        return await this.redeRepository.delete(rede_uid)
    }

    async update(redeEntity: RedeEntity){
        return this.redeRepository.update(redeEntity.rede_uid, redeEntity)
    }
}
