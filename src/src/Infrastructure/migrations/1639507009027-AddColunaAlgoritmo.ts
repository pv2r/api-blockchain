import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm'

export class AddColunaAlgoritmo1639507009027 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn("carteiras",new TableColumn(
            {
                name: 'algoritmo_uid',   
                type: 'varchar',
                generationStrategy: 'uuid'
            }
        ))
        await queryRunner.addColumn("redes",new TableColumn(
            {
                name: 'algoritmo_uid',   
                type: 'varchar',
                generationStrategy: 'uuid'
            }
        ))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("carteiras", "algoritmo_uid");
        await queryRunner.dropColumn("redes", "algoritmo_uid");
    }
}
