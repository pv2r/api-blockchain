import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class TabelasBase1639507009024 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'redes',
            columns: [
                {
                    name: 'rede_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "nome",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "nome_exibicao",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "moeda_principal",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'deleted_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: null,
                },
            ]
        }), true)
        await queryRunner.createTable(new Table({
            name: 'transacoes',
            columns: [
                {
                    name: 'transacao_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "to",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "from",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "hash",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "data",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'deleted_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: null,
                },
            ]
        }), true)
        await queryRunner.createTable(new Table({
            name: 'carteiras',
            columns: [
                {
                    name: 'carteira_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "endereco",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "chave_privada",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "chave_publica",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'deleted_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: null,
                },
            ]
        }), true)
        await queryRunner.createTable(new Table({
            name: 'carteira_rede',
            columns: [
                {
                    name: 'carteira_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'rede_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
            ]
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('redes', true)
        await queryRunner.dropTable('transacoes', true)
        await queryRunner.dropTable('carteiras', true)
        await queryRunner.dropTable('carteira_rede', true)
    }
}
