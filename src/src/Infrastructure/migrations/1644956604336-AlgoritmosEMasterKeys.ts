import { randomBytes } from "crypto";
import {MigrationInterface, QueryRunner} from "typeorm";
import { v4 as uuidv4 } from 'uuid'
import {ethers} from "ethers";


export class AlgoritmosEMasterKeys1644956604336 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        const algoritmoTeste = await queryRunner.query('select * from algoritmos where nome = "ECDSA"')

        let algoritmo_uid = uuidv4()
        if(algoritmoTeste.length == 0){
            await queryRunner.query(`insert into algoritmos (algoritmo_uid, nome, nome_exibicao, created_by) values('${algoritmo_uid}', 'ECDSA', 'Elliptic Curve Digital Signature Algorithm', 'Fintools')`)
        } else {
            algoritmo_uid = algoritmoTeste[0].algoritmo_uid
        } 
    
        const uid = uuidv4()
        const algoritmos_masters_uid = uuidv4()
        const mnemonic = ethers.Wallet.createRandom().mnemonic
        const masterKey = ethers.Wallet.fromMnemonic(mnemonic.phrase).privateKey
        await queryRunner.query(`insert into master_keys (uid, master_key, mnemonic) values ("${uid}", "${masterKey}", "${mnemonic.phrase}");`)
        await queryRunner.query(`insert into algoritmos_master_keys (uid, algoritmo_uid, master_key_uid) values("${algoritmos_masters_uid}", "${algoritmo_uid}", "${uid}")`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
