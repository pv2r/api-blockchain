import {MigrationInterface, QueryRunner} from "typeorm";

export class RedesSeeder1639507009024 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT into redes 
        (rede_uid, nome, nome_exibicao, moeda_principal, created_by, updated_by, deleted_by, created_at, updated_at, deleted_at)
        VALUES('6565d819-1870-4c45-aafd-cab68815cc6a', 'ethereum', 'Ethereum', 'ETH', 'Fintools', null, null, CURRENT_TIMESTAMP(6), CURRENT_TIMESTAMP(6), null);`);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
