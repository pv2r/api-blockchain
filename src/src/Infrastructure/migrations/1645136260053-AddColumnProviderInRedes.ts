import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddColumnProviderInRedes1645136260053 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn("redes",new TableColumn(
            {
                name: 'provider',   
                type: 'varchar',
                default: null
            }
        ))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("redes", "provider");
    }

}
