import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class TabelaAlgoritmo1639507009025 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'algoritmos',
            columns: [
                {
                    name: 'algoritmo_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "nome",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "nome_exibicao",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'deleted_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: null,
                },
            ]
        }), true)
        
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('algoritmos', true)
    }
}
