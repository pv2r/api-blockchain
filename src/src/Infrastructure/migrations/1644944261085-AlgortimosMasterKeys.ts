import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class AlgoritmosMasterKeys1644944261085 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'algoritmos_master_keys',
            columns: [
                {
                    name: 'uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "algoritmo_uid",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "master_key_uid",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
            ]
        }), true)

        await queryRunner.createForeignKey("algoritmos_master_keys", new TableForeignKey({
            columnNames: ["algoritmo_uid"],
            referencedColumnNames: ["algoritmo_uid"],
            referencedTableName: "algoritmos",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("algoritmos_master_keys", new TableForeignKey({
            columnNames: ["master_key_uid"],
            referencedColumnNames: ["uid"],
            referencedTableName: "master_keys",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('algoritmos_master_keys', true)
    }

}
