import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class DeleteCarteiraRedes1639507009026 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('carteira_rede', true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'carteira_rede',
            columns: [
                {
                    name: 'carteira_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'rede_uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
            ]
        }), true)
    }
}
