import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class MasterKeyTable1644943670161 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'master_keys',
            columns: [
                {
                    name: 'uid',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: "master_key",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: "mnemonic",
                    type: 'varchar',
                    length: '255',
                    isNullable: false,
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'deleted_by',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'now()',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: null,
                }
            ]
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('master_keys', true)

    }

}
