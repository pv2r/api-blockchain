import { IsNotEmpty, IsString } from "class-validator";
import { AlgoritmoEntity } from "../../Domain/Entity/algoritmo.entity";

export class RedeModel {
    @IsString()
    @IsNotEmpty()
    nome: string

    @IsString()
    @IsNotEmpty()
    moeda_principal: string

    @IsNotEmpty()
    algoritmo_uid: AlgoritmoEntity

    
    constructor(
        public rede_uid: string,
        nome: string,
        public nome_exibicao: string,
        moeda_principal: string,
        algoritmo_uid: AlgoritmoEntity,
        provider: string,
        public created_by?: string,
        public updated_by?: string,
        public deleted_by?: string,
    ){
        this.nome = nome
        this.moeda_principal = moeda_principal
        this.algoritmo_uid = algoritmo_uid
    }
    toObject() {
        return {...this}
    }

    transformaNomeEmNomeExibicao(){
        this.nome_exibicao = this.nome
    }
}