export class TransacaoModel {
    constructor(
        public to: string,
        public from: string,
        public hash: string,
        public data: string,
    ){}
    toObject() {
        return {...this}
    }
}