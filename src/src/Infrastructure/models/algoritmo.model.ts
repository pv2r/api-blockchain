export class AlgoritmoModel {
    constructor(
        public algoritmo_uid: string,
        public nome: string,
        public nome_exibicao: string,
        public created_by?: string,
        public updated_by?: string,
        public deleted_by?: string,
    ){}
    toObject() {
        return {...this}
    }
}