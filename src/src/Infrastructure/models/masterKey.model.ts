import { AlgoritmoEntity } from "../../Domain/Entity/algoritmo.entity";

export class MasterKeyModel {
    constructor(
        public master_key?: string,
        public algoritmo_uid?: string
    ){}
    toObject() {
        return {...this}
    }

    getWhereAlgoritmo(){
        return {
            algoritmos: {
                algoritmo_uid: this.algoritmo_uid
            }
        }
    }
}