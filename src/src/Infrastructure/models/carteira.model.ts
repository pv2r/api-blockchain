import { AlgoritmoEntity } from "../../Domain/Entity/algoritmo.entity";

export class CarteiraModel {
    constructor(
        public endereco: string,
        public chave_publica: string,
        public chave_privada: string,
        public algoritmo_uid: AlgoritmoEntity,
        public created_by?: string,
        public updated_by?: string,
        public deleted_by?: string,
    ){}
    toObject() {
        return {...this}
    }
}