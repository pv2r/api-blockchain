export default () => ({
    port: parseInt(process.env.PORT, 10) || 3000,
    mysql: {
        type: process.env.API_WALLET_DATABASE_TYPE,
        host: process.env.API_WALLET_DATABASE_HOST,
        port: process.env.API_WALLET_DATABASE_PORT,
        username: process.env.API_WALLET_DATABASE_USERNAME,
        password: process.env.API_WALLET_DATABASE_PASSWORD,
        database: process.env.API_WALLET_DATABASE,
    }
  });