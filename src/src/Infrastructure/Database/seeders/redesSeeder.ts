import { getConnection } from "typeorm";
import {SeederInterface} from "./seederInterface";
import {RedeEntity} from "../../../Domain/Entity/rede.entity";

export class RedesSeeder implements SeederInterface {
    async create(){
        const data = [
            {
                nome: "ethereum",
                nome_exibicao: "Ethereum",
                created_by: "Fintools",
                moeda_principal: "ETH"
            }
        ]
        console.log("Gravando redes padrão")
        await getConnection()
        .createQueryBuilder()
        .insert()
        .into(RedeEntity)
        .values(data)
        .execute();
        console.log("redes gravadas")
    }
}