import { NestFactory } from "@nestjs/core";
import { AppModule } from "src/app.module";
import { RedesSeeder } from "./seeders/redesSeeder";

async function bootstrap() {
  NestFactory.createApplicationContext(AppModule)
    .then(appContext => {
        const seeders = [RedesSeeder]

        seeders.forEach(seeder => {
          new seeder().create()
        });
        
      })
    
  }
bootstrap();