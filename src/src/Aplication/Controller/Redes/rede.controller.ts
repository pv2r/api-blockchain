import { Body, Controller, Delete, Get, Post, Res, HttpStatus } from '@nestjs/common';
import { RedeEntity } from '../../../Domain/Entity/rede.entity';
import { RedeService } from '../../../Domain/Services/Rede/rede.service';
import { RedeModel } from '../../../Infrastructure/models/rede.model';
import { BaseApiService } from '../../../Utils/Api/baseApi.service';
import { RespostaDto } from '../../../Utils/Api/response.dto';
import { Response } from 'express';

@Controller({
    version: '1',
    path: 'redes'
})
export class RedeController {
    constructor(
        private redeService: RedeService,
        private baseApiService: BaseApiService
    ){}

    @Get()
    index(@Res({ passthrough: true }) res: Response): any {
        try{
            return this.redeService.findAll();
        }catch(e){
            res.status(HttpStatus.BAD_REQUEST);
            return this.baseApiService.resposta([],e.reason, false);
        }
    }

    @Post()
    async store(@Body() rede: RedeModel, @Res({ passthrough: true }) res: Response): Promise<RespostaDto> {
        try{
            return this.baseApiService.resposta(await this.redeService.store(rede), 'Gravado com sucesso', true)
        }catch(e){
            res.status(HttpStatus.BAD_REQUEST);
            return this.baseApiService.resposta([],e.reason, false);
        }
    }

    @Delete()
    async destroy(@Body() rede_uid: string, @Res({ passthrough: true }) res: Response): Promise<RespostaDto> {
        try{
            return this.baseApiService.resposta(await this.redeService.delete(rede_uid), "Deletado com sucesso", true)
        }catch(e){
            res.status(HttpStatus.BAD_REQUEST);
            return this.baseApiService.resposta([],e.reason, false);
        }
    }
}
