import { BaseApiService } from "../../../Utils/Api/baseApi.service";
import { Body, Controller, Post, HttpStatus, Res} from "@nestjs/common";
import {Response} from 'express';
import {AssinarTransacaoValidate} from "./Requests/assinarTransacao.request";
import {AssinarTransacaoAction} from "../../../Domain/Services/Transacao/assinar-transacao-action.service";
import {TransacaoDto} from "../../../Domain/Services/Transacao/Dto/transacao.dto";

@Controller({
    version: '1',
    path: 'transacao'
})
export class TransacaoController {
    constructor(
        private assinarTransacaoAction: AssinarTransacaoAction,
        private baseApiAService: BaseApiService) {}

    @Post('/assinar')
    async store(@Body() transacao: TransacaoDto, @Res({ passthrough: true }) res: Response){
        try{
            return await this.assinarTransacaoAction.executar(transacao)
        }catch(e){
            res.status(HttpStatus.BAD_REQUEST);
            return this.baseApiAService.resposta([],e.reason, false)
        }
        
    }
}