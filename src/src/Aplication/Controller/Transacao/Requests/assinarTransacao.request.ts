import { Type } from "class-transformer"
import {IsObject, ValidateNested} from "class-validator"
import {TransacaoDto} from "../../../../Domain/Services/Transacao/Dto/transacao.dto";

export class AssinarTransacaoValidate {
    @IsObject()
    @ValidateNested()
    @Type(() => TransacaoDto)
    transacao: TransacaoDto
}


