import { Body, Controller, Post } from "@nestjs/common";
import { StoreDto } from "../../../Domain/Services/Blockchain/store.dto";
import {BlockchainService} from "../../../Domain/Services/Blockchain/blockchain.service";

@Controller({
    version: '1',
    path: 'blocos'
})
export class BlockchainController {
    constructor(private blockchainService: BlockchainService){}

    @Post()
    async store(@Body() storeDto: StoreDto){
        return await this.blockchainService.enviarBloco(storeDto.transacao)
    }
}