import { Module } from '@nestjs/common';
import { CarteiraController } from './Carteira/carteira.controller';
import { UtilsModule } from '../../Utils/utils.module';
import { RedeController } from './Redes/rede.controller';
import { RepositoryModule } from '../../Infrastructure/Repository/repository.module';
import { DomainModule } from '../../Domain/domain.module';
import { BlockchainController } from './Blockchain/blockchain.controller';
import { HealthCheckController } from './HealthCheck/healthcheck.controller';
import {TransacaoController} from "./Transacao/transacao.controller";
import { MasterKeyController } from './MasterKey/masterKey.controller';

@Module({
  controllers: [
    CarteiraController,
    RedeController,
    BlockchainController,
    HealthCheckController,
    TransacaoController,
    MasterKeyController
  ],
  imports: [RepositoryModule, UtilsModule, DomainModule],
})
export class ControllerModule {}
