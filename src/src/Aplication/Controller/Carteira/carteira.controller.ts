import { BaseApiService } from "../../../Utils/Api/baseApi.service";
import {Body, Controller, Get, Inject, Post, Query, Res, HttpStatus} from '@nestjs/common';
import { CarteiraRepository } from "../../../Infrastructure/Repository/Carteira/carteira.repository";
import { RespostaDto } from "../../../Utils/Api/response.dto";
import {Response} from 'express';
import { GerarCarteiraService } from "../../../Domain/Services/GerarCarteira/gerarCarteira.service";
import {CarteiraDto} from "../../../Domain/Services/GerarCarteira/carteira.dto";

@Controller({
  version: '1',
  path: 'carteiras'
})
export class CarteiraController {
  constructor(
    private carteiraRepository: CarteiraRepository,
    private baseApiAService: BaseApiService,
    @Inject('GerarCarteiraInterface')
    private gerarCarteiraService: GerarCarteiraService
  ) { }

  @Get()
  async index(@Body() carteiraDto: CarteiraDto, @Res({ passthrough: true }) res: Response): Promise<RespostaDto> {
    try{
      const data = await this.carteiraRepository.findAll(carteiraDto);
      return this.baseApiAService.resposta(data, "Dados recuperados com sucesso", true)
    }catch(e){
      res.status(HttpStatus.BAD_REQUEST);
      return this.baseApiAService.resposta([],e.reason, false);
    }
  }

  @Get('chave_publica')
  async show(@Body() carteiraDto: CarteiraDto, @Res({ passthrough: true }) res: Response): Promise<RespostaDto> {
    try{
      const carteira = await this.carteiraRepository.findOne(carteiraDto);
      const mapFunction = this.carteiraRepository.mapeiaResposta([carteira], {
        chave_publica: carteira.chave_publica,
        endereco: carteira.endereco
      })
      return this.baseApiAService.respostaPersonalizada([carteira], mapFunction, "Dados recuperados com sucesso", true)
    }catch(e){
      res.status(HttpStatus.BAD_REQUEST);
      return this.baseApiAService.resposta([],e.reason, false);
    }
    
  }

  @Post()
  async store(@Body() carteiraDto: CarteiraDto, @Res({ passthrough: true }) res: Response): Promise<RespostaDto> {
    try{
      const data = await this.gerarCarteiraService.gerarCarteira(carteiraDto.algoritmo, "fintools")
      return this.baseApiAService.resposta(data, "Dados recuperados com sucesso", true)
      
    }catch(e){
      res.status(HttpStatus.BAD_REQUEST);
      return this.baseApiAService.resposta([],e.reason, false);
  }
  }
}
