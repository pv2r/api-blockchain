import { Controller, Get } from '@nestjs/common';
import { createConnection } from 'typeorm';

@Controller('healthcheck')
export class HealthCheckController {
  @Get()
  async index(): Promise<any> {
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const connection = await createConnection({
        name: 'healthcheck',
        type: 'mysql',
        host: process.env.API_WALLET_DATABASE_HOST,
        port: process.env.API_WALLET_DATABASE_PORT,
        username: process.env.API_WALLET_DATABASE_USERNAME,
        password: process.env.API_WALLET_DATABASE_PASSWORD,
        database: process.env.API_WALLET_DATABASE,
      });
      return {
        project: 'API-BLOCKCHAIN',
        healthy: true,
        database: {
          description: 'Checa conexão com banco de dados.',
          healthy: connection.isConnected,
        },
      };
    } catch (e) {
      return {
        project: 'API-BLOCKCHAIN',
        healthy: false,
        database: {
          description: 'Checa conexão com banco de dados.',
          healthy: false,
        },
      };
    }
  }
}
