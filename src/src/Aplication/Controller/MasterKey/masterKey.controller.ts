import { BaseApiService } from "../../../Utils/Api/baseApi.service";
import {Body, Controller, Get, Inject, Post, Query} from '@nestjs/common';
import { CarteiraRepository } from "../../../Infrastructure/Repository/Carteira/carteira.repository";
import { RespostaDto } from "../../../Utils/Api/response.dto";
import { GerarCarteiraService } from "../../../Domain/Services/GerarCarteira/gerarCarteira.service";
import {CarteiraDto} from "../../../Domain/Services/GerarCarteira/carteira.dto";
import { MasterKeyRepository } from "../../../Infrastructure/Repository/MasterKey/masterKey.repository";
import { MasterKeyEntity } from "../../../Domain/Entity/masterKey.entity";
import { MasterKeyService } from "../../../Domain/Services/MasterKey/masterKey.service";

@Controller({
  version: '1',
  path: 'master_keys'
})
export class MasterKeyController {
  constructor(
    private masterKeyService: MasterKeyService,
  ) { }

  @Get()
  async index(): Promise<MasterKeyEntity[]> {
    return await this.masterKeyService.findAll()
  }

}
