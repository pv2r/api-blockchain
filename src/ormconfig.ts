export default {
    "type": "mysql",
    "host": process.env.API_WALLET_DATABASE_HOST,
    "port": process.env.API_WALLET_DATABASE_PORT,
    "username": process.env.API_WALLET_DATABASE_USERNAME,
    "password": process.env.API_WALLET_DATABASE_PASSWORD,
    "database": process.env.API_WALLET_DATABASE,
    "entities": [],
    "migrationsTableName": process.env.API_BLOCKCHAIN_MIGRATIONS_TABLE,
    "migrations": [process.env.API_BLOCKCHAIN_MIGRATIONS_RUN],
    "cli": {
        "migrationsDir": process.env.API_BLOCKCHAIN_MIGRATIONS_DIR
    }
};