import {INestApplication} from "@nestjs/common";
import {Test} from "@nestjs/testing";
import {AppModule} from "../src/app.module";
import * as request from 'supertest';
import { getEntityManagerToken, TypeOrmModule } from "@nestjs/typeorm";
import { CarteiraPersistenceModule } from "../src/Infrastructure/Repository/Carteira/carteira.persistence.module";
import { CarteiraRepository } from "../src/Infrastructure/Repository/Carteira/carteira.repository";
import { CarteiraDto } from "../src/Domain/Services/GerarCarteira/carteira.dto";
import { AlgoritmoEntity } from "../src/Domain/Entity/algoritmo.entity";
import { DomainModule } from "../src/Domain/domain.module";
import { RepositoryModule } from "../src/Infrastructure/Repository/repository.module";
import { ControllerModule } from "../src/Aplication/Controller/controller.module";
import {Connection, createConnection, EntityManager, getManager} from "typeorm";

describe('Carteiras', () => {
    let app: INestApplication;
    let carteiraRepository: CarteiraRepository;
    let connection: Connection;
    let manager: EntityManager;
    const mockCarteirasUids: Array<string> = [
        '5ec50b7f-b96b-4d78-8c4c-6f41afa083c9',
        '9e021318-302a-496a-8d84-e95a360b5d65'
    ]
    const algoritmo_uid: string = 'aff988f3-ef00-4d7a-bd57-392f3bdee79e'
    

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                DomainModule,
                RepositoryModule,
                ControllerModule,
                TypeOrmModule.forRoot({
                    name: 'default',
                    type: 'mysql',
                    host: 'blockchain-db',
                    port: 3306,
                    username: 'root',
                    password: 'root',
                    database: 'api_blockchain',
                    entities: ['./**/*.entity.ts'],
                    synchronize: false,
                }),
            ],
            providers: [
                CarteiraPersistenceModule,
            ]
        }).compile();

        app = moduleRef.createNestApplication();
        await app.init();

        manager = getManager('default')
      
        await manager.query(`insert into algoritmos (algoritmo_uid, nome, nome_exibicao) values('${algoritmo_uid}', 'teste', 'teste')`)
        mockCarteirasUids.forEach(async (carteira_uid) => {
            await manager.query(`insert into carteiras (carteira_uid, endereco, chave_privada, chave_publica, algoritmo_uid) values('${carteira_uid}', 'teste', 'teste', 'teste', '${algoritmo_uid}')`)
        })                    
    });

    it(`Deve retornar um array vazio ao listar carteiras`, async () => {
        
        const resultado = await manager.query("select * from carteiras where endereco='naoexistente'")

        expect(resultado).toStrictEqual([])
        expect(resultado).toEqual([])
    });

    it(`Deve retornar um array com 2 registros ao listar carteiras`, async () => {
        
        const carteiras = await manager.query("select * from carteiras where endereco = 'teste' and chave_publica = 'teste'")

        expect(carteiras).toBeDefined()
        expect(carteiras).toHaveLength(2)
    });

    it(`Deve retornar 200 ao listar carteiras`, () => {
        return request(app.getHttpServer())
            .get('/carteiras')
            .expect(200)
    });

    afterAll(async () => {
        mockCarteirasUids.forEach(async (carteira_uid) => {
            await manager.query(`delete from carteiras where carteira_uid = '${carteira_uid}'`)
        })   

        await manager.query(`delete from algoritmos where algoritmo_uid = '${algoritmo_uid}'`)

        await manager.connection.close()
        await app.close();
    });
});