import {INestApplication} from "@nestjs/common";
import {Test} from "@nestjs/testing";
import {AppModule} from "../src/app.module";
import * as request from 'supertest';

describe('Redes', () => {
    let app: INestApplication;
    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [AppModule],
        }).compile()
        app = moduleRef.createNestApplication();
        await app.init()
    })
    it(`Deve retornar 200 ao listar redes`, () => {
        const response = request(app.getHttpServer()).get('/redes')
        response.expect(200)
    })
    afterAll(async () => {
        await app.close()
    })
});
