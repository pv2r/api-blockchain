FROM node:14.15.4-alpine3.12 AS base

RUN apk add --no-cache bash shadow

RUN echo "PS1='\s-\v \$PWD \$ '" >> /home/node/.bashrc \
    && mkdir /docker && chown node:node /docker \
    && mkdir /home/node/app && chown node:node /home/node/app \
    && usermod -u 1000 node

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

RUN npm install -g @nestjs/cli@8.0.0

WORKDIR /home/node/app
COPY --chown=1000:node ./docker/app/entrypoint.sh /docker/
COPY --chown=1000:node ./src/ .


FROM base AS test
WORKDIR /home/node/app
RUN npm run test
RUN npm run test:e2e


FROM base AS prod
WORKDIR /home/node/app
USER node
ARG ENV
RUN cp -vf .env.$ENV .env \
    && rm -rf .env.* \
    && chown -R node:node .env
EXPOSE 3000
CMD dockerize /docker/entrypoint.sh