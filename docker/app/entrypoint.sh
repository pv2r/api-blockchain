#!/bin/bash

#if [ ! -f ".env" ]; then
#    cp .env.example .env
#fi

# Pipeline
npm install
npm run prebuild
npm run build
npm run typeorm migration:run
npm run start:prod
